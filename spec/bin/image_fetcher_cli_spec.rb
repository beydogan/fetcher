require 'spec_helper'
require 'open3'

# @TODO
#   - Can integrate VCR to avoid making real HTTP requests.
describe 'Image Fetcher Script' do
  context 'when options are valid' do
    let(:cmd) { 'ruby bin/image_fetcher_cli --input spec/fixtures/input.txt --output tmp/downloads' }

    after do
      File.delete('tmp/downloads/150x150.png')
      File.delete('tmp/downloads/300x300.jpg')
    end

    it 'fetches images and downloads, prints out errors' do
      output = nil

      Open3.popen3(cmd) do |_, stdout, _stderr|
        output = stdout.read
      end

      expect(output).to include 'foo is not a valid url'
      expect(output).to include 'http is not a valid url'
      expect(output).to include 'https:// foobar.com/ is not a valid url'
      expect(File.file?('tmp/downloads/150x150.png')).to eq true
      expect(File.file?('tmp/downloads/300x300.jpg')).to eq true
    end
  end

  context 'when input option is missing' do
    let(:cmd) { 'ruby bin/image_fetcher_cli' }

    it 'returns error' do
      output = nil

      Open3.popen3(cmd) do |_, _, stderr|
        output = stderr.read
      end

      expect(output).to include 'input is missing'
    end
  end

  context 'when output option is missing' do
    xit 'returns error'
  end
end
