require 'spec_helper'
require 'image_list_downloader'

describe ImageListDownloader do
  subject { ImageListDownloader.new(urls, output_location, force) }

  let(:urls) { %w[http://foobar.com/image1.png http://foobar.com/image2.png] }
  let(:output_location) { 'tmp/downloads' }
  let(:force) { true }

  describe '#run!' do
    before do
      allow(subject).to receive(:download!)
    end

    it 'calls #download for each urls' do
      subject.run!

      expect(subject).to have_received(:download!).with(urls[0])
      expect(subject).to have_received(:download!).with(urls[1])
    end
  end

  describe '#download!' do
    let(:tempfile) { instance_double(Tempfile, close: nil, path: '/tmp/random.jpg') }
    let(:url) { urls[0] }

    context 'when download is successful' do
      let(:mime) { double(image?: true) }

      before do
        allow(Down::NetHttp).to receive(:download).and_return(tempfile)
        allow(FileUtils).to receive(:mv)
        allow(MimeMagic).to receive(:by_magic).and_return(mime)
      end

      context 'when force is true' do
        let(:force) { false }

        it 'downloads the file from given url to output location' do
          subject.send(:download!, url)

          expect(Down::NetHttp).to have_received(:download).with(url, max_redirects: 5)
          expect(FileUtils).to have_received(:mv).with(tempfile.path, 'tmp/downloads/image1.png')
        end
      end

      context 'when force is false' do
        let(:force) { false }

        xit 'does not download if file already exists'
      end
    end

    context 'when download failed' do
      let(:exception) { Down::Error.new('Something went wrong') }

      before do
        allow(Down::NetHttp).to receive(:download).and_raise(exception)
      end

      it 'catches the exception and logs' do
        expect do
          subject.send(:download!, url)
        end.to output("Error while downloading: #{url}, #{exception.message}\n").to_stdout
      end
    end

    context 'when given file is not an image' do
      xit 'skips downloading and logs'
    end
  end

  describe '#image?' do
    xit 'returns true if given file is an image'
  end
end
