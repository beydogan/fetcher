require 'spec_helper'
require 'input_file_parser'

describe InputFileParser do
  subject { InputFileParser.new('spec/fixtures/input.txt').run }

  it 'parses urls form the input file' do
    expect(subject).to eq [
      'http://placehold.jp/150x150.png',
      'https://placehold.jp/300x300.jpg',
      'https://www.google.com'
    ]
  end

  it 'outputs warnings' do
    warnings = [
      "#1 foo is not a valid url\n",
      "#2 http is not a valid url\n",
      "#3 https:// foobar.com/ is not a valid url\n"
    ].join('')

    expect { subject }.to output(warnings).to_stdout
  end
end
