# Image Fetcher

Simple Ruby CLI to download images from given input file to output folder.

**Note:** There could be many cases and features to be added but due to lack of time 
everything I've thought have been documented in each class under the lib folder.

### Prerequisites

- Ruby 2.7.0 or greater

### Setup

```
git clone git@gitlab.com:beydogan/fetcher.git
cd fetcher
bundle install
```

### Usage

```
Usage:
  image_fetcher_cli

Options:
  --input=VALUE                   	# Input file location to read URLS
  --output=VALUE                  	# Output destination to download images
  --[no-]force                    	# Overwrites if file already exists when true, default: true
  --help, -h                      	# Print this help
```

#### Example

```
bin/image_fetcher_cli --input spec/fixtures/input.txt --output downloads
```

#### Help
```
bin/image_fetcher_cli --help
```

### Running the tests

`rspec`
