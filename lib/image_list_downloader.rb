require 'down/net_http'
require 'mimemagic'
# Downloads given urls to given folder.
#
# It assumes URLs are valid and output folder is present.
# It follows the redirects but respects to the filename of the first url.
# It doesn't break if a URL fails to download but outputs a warning message and continues.
#
# @TODO
#   - Implement 'force' logic, it shouldn't download if it's false and file already exists
#   - Some files might not have an extension, it can be improved by determining
#     the content type and adding the file extension to the file name.
#   - For reusability, it can be named as FileListDownload and take `Array<String> mime_types` param to whitelist
#     which file types to be downloaded.
#   - Possible improvement might be extracting #download method into a FileDownloader class
#     for single responsibility principle also easier testing.
#   - Depending on the requirements of the application, it can be optimized by checking the content-type for image
#     before downloading it
class ImageListDownloader
  # @param [Array<String>] urls to be downloaded
  # @param [String] output destination for downloads
  # @param [Boolean] force overwrite or not if file already exists(not implemented)
  def initialize(urls, output, force = true)
    @urls = urls
    @output = output
    @force = force
  end

  def run!
    @urls.collect { |url| download!(url) }
  end

  private

  # @param [String] url to be downloaded
  #
  # @param [Boolean]
  def download!(url)
    uri = URI.parse(url)
    filename = File.basename(uri.path)

    tempfile = Down::NetHttp.download(url, max_redirects: 5)

    raise StandardError, "#{url} is not an image, skipped downloading" unless image?(tempfile)

    tempfile.close

    FileUtils.mv tempfile.path, File.join(@output, filename)

    true
  rescue Down::Error => e
    puts "Error while downloading: #{url}, #{e.message}"

    false
  rescue StandardError => e
    puts e.message

    false
  end

  # @param [File] file to be checked
  def image?(file)
    MimeMagic.by_magic(file).image?
  end
end
