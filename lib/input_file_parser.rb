# Parses given input file path for matching valid urls per line and returns urls.
#
# It assumes file exists.
# It outputs warning message for non valid urls
#
# @TODO
#   - Can log warnings into a logger instead of stdout
class InputFileParser
  URL_REGEX = %r{^(http|https)://[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,}(:[0-9]{1,5})?(/.*)?$}ix.freeze

  # @param [String] file path to parse urls.
  def initialize(file)
    @file = file
  end

  # @return [Array<String>] array of valid URLs
  def run
    File.open(@file).readlines.each_with_index.filter_map do |line, i|
      value = line.chomp

      unless value.match?(URL_REGEX)
        puts "##{i + 1} #{value} is not a valid url"
        next
      end

      value
    end
  end
end
