require_relative './image_list_downloader'
require_relative './input_file_parser'
require 'uri'
require 'fileutils'

# Entry point to the app which downloads files from given input file to output file
#
# @TODO
#   - Implement custom error class instead of StandardError to exceptions
#   - Implement custom logger with an option to print out to STDOUT or a file
#   - Implement 'fail fast' logic, right now it skips and continues in case of an invalid URL or download error
#   - Implement file type check for input file
#   - Can have a file limit option
#   - Can log info messages for downloading what is downloaded to where
#   - Can raise error back to ImageFetcherCli so script can have non-zero exit code
class ImageFetcher
  # @param [String] input file location to read URLS
  # @param [String] output destination to download images
  # @param [Boolean] force overwrite or not if file already exists
  def initialize(input:, output:, force: true)
    @input = input
    @output = output
    @force = force

    check_file!
    prepare_output!
  rescue StandardError => e
    puts e.message
    @error = true
  end

  def run!
    return if @error

    urls = InputFileParser.new(@input).run
    ImageListDownloader.new(urls, @output, @force).run!
  end

  private

  def check_file!
    raise StandardError, 'input argument is missing.' if @input.nil?
    raise StandardError, 'input file is not found.' unless File.file?(@input)
  end

  def prepare_output!
    raise StandardError, 'output argument is missing.' if @output.nil?

    FileUtils.mkdir_p(@output) unless File.exist?(@output)
  rescue Errno::EROFS
    raise StandardError, 'Permission denied for output folder creation.'
  rescue Errno::ENOENT
    raise StandardError, 'output path cannot be found.'
  end
end
